#include "stdafx.h"
#include "buffer.h"
#include "Math.h"
namespace iocp
{
    namespace pipe
    {
        buffer::buffer()
        {
            // Initial size
            m_nSize = 0;

            m_pPtr = m_pBase = NULL;
        }
        buffer::~buffer()
        {
            if (m_pBase) {
                VirtualFree(m_pBase, 0, MEM_RELEASE);
                m_pBase = NULL;
            }
        }

        BOOL buffer::Write(PBYTE pData, UINT nSize)
        {
            ReAllocateBuffer(nSize + GetBufferLen());

            CopyMemory(m_pPtr, pData, nSize);

            // Advance Pointer
            m_pPtr += nSize;

            return nSize;
        }

        BOOL buffer::Insert(PBYTE pData, UINT nSize)
        {
            ReAllocateBuffer(nSize + GetBufferLen());

            MoveMemory(m_pBase + nSize, m_pBase, GetMemSize() - nSize);
            CopyMemory(m_pBase, pData, nSize);

            // Advance Pointer
            m_pPtr += nSize;

            return nSize;
        }

        UINT buffer::Read(PBYTE pData, UINT nSize)
        {
            // Trying to byte off more than ya can chew - eh?
            if (nSize > GetMemSize()) return 0;

            // all that we have
            if (nSize > GetBufferLen()) nSize = GetBufferLen();

            if (nSize) {
                // Copy over required amount and its not up to us
                // to terminate the buffer - got that!!!
                CopyMemory(pData, m_pBase, nSize);

                // Slide the buffer back - like sinking the data
                MoveMemory(m_pBase, m_pBase + nSize, GetMemSize() - nSize);

                m_pPtr -= nSize;
            }

            DeAllocateBuffer(GetBufferLen());

            return nSize;
        }

        UINT buffer::GetMemSize()
        {
            return m_nSize;
        }

        UINT buffer::GetBufferLen()
        {
            if (m_pBase == NULL) return 0;

            int nSize = (int)(m_pPtr - m_pBase);
            return nSize;
        }

        UINT buffer::ReAllocateBuffer(UINT nRequestedSize)
        {
            try
            {

                if (nRequestedSize < GetMemSize()) return 0;

                // Allocate new size
                UINT nNewSize = (UINT)ceil(nRequestedSize / 1024.0) * 1024;

                // New Copy Data Over
                PBYTE pNewBuffer =
                    (PBYTE)VirtualAlloc(NULL, nNewSize, MEM_COMMIT, PAGE_READWRITE);

                UINT nBufferLen = GetBufferLen();
                CopyMemory(pNewBuffer, m_pBase, nBufferLen);

                if (m_pBase) VirtualFree(m_pBase, 0, MEM_RELEASE);

                // Hand over the pointer
                m_pBase = pNewBuffer;

                // Realign position pointer
                m_pPtr = m_pBase + nBufferLen;

                m_nSize = nNewSize;
            }
            catch (...)
            {
            }

            return m_nSize;
        }

        UINT buffer::DeAllocateBuffer(UINT nRequestedSize)
        {
            if (nRequestedSize < GetBufferLen()) return 0;

            // Allocate new size
            UINT nNewSize = (UINT)ceil(nRequestedSize / 1024.0) * 1024;

            if (nNewSize < GetMemSize()) return 0;

            // New Copy Data Over
            PBYTE pNewBuffer =
                (PBYTE)VirtualAlloc(NULL, nNewSize, MEM_COMMIT, PAGE_READWRITE);

            UINT nBufferLen = GetBufferLen();
            CopyMemory(pNewBuffer, m_pBase, nBufferLen);

            VirtualFree(m_pBase, 0, MEM_RELEASE);

            // Hand over the pointer
            m_pBase = pNewBuffer;

            // Realign position pointer
            m_pPtr = m_pBase + nBufferLen;

            m_nSize = nNewSize;

            return m_nSize;
        }

        int buffer::Scan(PBYTE pScan, UINT nPos)
        {
            if (nPos > GetBufferLen()) return -1;

            PBYTE pStr = (PBYTE)strstr((char*)(m_pBase + nPos), (char*)pScan);

            int nOffset = 0;

            if (pStr) nOffset = (int)(pStr - m_pBase) + (int)strlen((char*)pScan);

            return nOffset;
        }

        void buffer::ClearBuffer()
        {
            // Force the buffer to be empty
            m_pPtr = m_pBase;

            DeAllocateBuffer(1024);
        }

        void buffer::Copy(buffer& buffer)
        {
            int nReSize = buffer.GetMemSize();
            int nSize = buffer.GetBufferLen();
            ClearBuffer();
            ReAllocateBuffer(nReSize);

            m_pPtr = m_pBase + nSize;

            CopyMemory(m_pBase, buffer.GetBuffer(), buffer.GetBufferLen());
        }

        PBYTE buffer::GetBuffer(UINT nPos)
        {
            return m_pBase + nPos;
        }

        UINT buffer::Delete(UINT nSize)
        {
            // Trying to byte off more than ya can chew - eh?
            if (nSize > GetMemSize()) return 0;

            // all that we have
            if (nSize > GetBufferLen()) nSize = GetBufferLen();

            if (nSize) {
                // Slide the buffer back - like sinking the data
                MoveMemory(m_pBase, m_pBase + nSize, GetMemSize() - nSize);

                m_pPtr -= nSize;
            }

            DeAllocateBuffer(GetBufferLen());

            return nSize;
        }
    }
}