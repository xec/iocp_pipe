#pragma once

#include <Windows.h>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/noncopyable.hpp>
#include <boost/pool/object_pool.hpp>
#include "Buffer.h"

namespace iocp
{

namespace pipe
{
namespace inl
{
enum IOType {
    IOInitialize,
    IORead,
    IOWrite,
    IOIdle,
    IOAccept,
};

typedef boost::function<void(int)> event_callback;
typedef boost::function<void(int, size_t)> io_event_callback;

class OVERLAPPEDPLUS
{
  public:
    OVERLAPPED m_Overlapped;
    IOType m_ioType;
    event_callback callback;
    io_event_callback io_callback;

    OVERLAPPEDPLUS(IOType ioType)
    {
        callback = NULL;
        io_callback = NULL;
        ZeroMemory(&m_Overlapped, sizeof(OVERLAPPED));
        m_Overlapped.hEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
        m_ioType = ioType;
    }

    ~OVERLAPPEDPLUS()
    {
        if (m_Overlapped.hEvent) CloseHandle(m_Overlapped.hEvent);
    }
};

//
//  Client Context
//
typedef struct _ClientContext
{
    HANDLE pipeHandle;
    LPVOID lpClientExtension;
} ClientContext;
}

class pipe;
class acceptor;
class reactor;
class pipe : public boost::noncopyable
{
  public:
    pipe(reactor& srv);
    virtual ~pipe();

    void stop();

    void async_read(char* buf, size_t len, inl::io_event_callback cb);
    void async_write(char* buf, size_t len, inl::io_event_callback cb);
    HANDLE& handle();

    std::wstring computer_name() const;
    ULONG remote_process_id() const;
    ULONG remote_session_id() const;

  private:
    reactor& reactor_;
    HANDLE handle_;
};
typedef boost::shared_ptr<pipe> session_ptr;

class acceptor : public boost::noncopyable
{
  public:
    friend class reactor;
    friend class pipe;
    acceptor(reactor& srv, const std::wstring& pipename, const size_t buffer_size);
    virtual ~acceptor();

    void async_accept(HANDLE& h, inl::event_callback cb);

  private:
    reactor& server_;
    std::wstring pipe_name_;
    size_t buffer_size_;
};
typedef boost::shared_ptr<acceptor> acceptor_ptr;

class reactor : public boost::noncopyable
{
  public:
    friend class acceptor;
    friend class pipe;

    reactor();
    virtual ~reactor();
    virtual void run();

  protected:
    void associate(HANDLE h, void* context);

  private:
    inl::OVERLAPPEDPLUS* alloc_ovl(inl::IOType type);
    void free_ovl(inl::OVERLAPPEDPLUS* p);

  private:
    HANDLE completion_port_;
    boost::object_pool<inl::OVERLAPPEDPLUS> overlapped_pool_;
};
}
}
