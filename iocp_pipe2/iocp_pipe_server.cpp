#include "stdafx.h"
#include "iocp_pipe_server.h"
#include <Windows.h>

namespace iocp
{
namespace pipe
{
pipe::pipe(reactor& srv) : reactor_(srv), handle_(INVALID_HANDLE_VALUE)
{
}

pipe::~pipe()
{
}

HANDLE& pipe::handle()
{
    return handle_;
}

void pipe::async_read(char* buf, size_t len, inl::io_event_callback cb)
{
    auto* pOverlap = reactor_.alloc_ovl(inl::IORead);
    pOverlap->io_callback = cb;
    DWORD byteRead = 0;
    DWORD dwError = 0;

    BOOL bRet =
        ReadFile(handle(), buf, len, &byteRead, &pOverlap->m_Overlapped);
    if (!bRet) {
        dwError = GetLastError();
        if (dwError != ERROR_IO_PENDING) {
            cb(dwError, 0);
        }
    }
}

void pipe::async_write(char* buf, size_t len, inl::io_event_callback cb)
{
    auto* pOverlap = reactor_.alloc_ovl(inl::IOWrite);
    pOverlap->io_callback = cb;
    DWORD byteWritten = 0;
    DWORD dwError = 0;

    BOOL bRet =
        WriteFile(handle(), buf, len, &byteWritten, &pOverlap->m_Overlapped);
    if (!bRet) {
        dwError = GetLastError();
        if (dwError != ERROR_IO_PENDING) {
            cb(dwError, 0);
        }
    }
}

std::wstring pipe::computer_name() const
{
    TCHAR szComputerName[256] = {0};
    GetNamedPipeClientComputerName(handle_, szComputerName, 256);
    return szComputerName;
}

ULONG pipe::remote_process_id() const
{
    ULONG ProcessId = 0;
    return GetNamedPipeClientProcessId(handle_, &ProcessId) ? ProcessId : 0;
}

ULONG pipe::remote_session_id() const
{
    ULONG SessionId = 0;
    return GetNamedPipeClientSessionId(handle_, &SessionId) ? SessionId : 0;
}

void pipe::stop()
{
    FlushFileBuffers(handle_);
    DisconnectNamedPipe(handle_);
    CloseHandle(handle_);
}

acceptor::acceptor(reactor& srv, const std::wstring& pipename,
                   const size_t buffer_size)
    : server_(srv)
{
    pipe_name_ = pipename;
    buffer_size_ = buffer_size;
}

acceptor::~acceptor()
{
}

void acceptor::async_accept(HANDLE& h, inl::event_callback cb)
{
    inl::OVERLAPPEDPLUS* Overlapped = server_.alloc_ovl(inl::IOAccept);
    Overlapped->callback = cb;
    BOOL fConnected = FALSE;
    h = CreateNamedPipe(pipe_name_.c_str(), PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED,
                        PIPE_TYPE_MESSAGE | PIPE_WAIT | PIPE_READMODE_MESSAGE,
                        PIPE_UNLIMITED_INSTANCES, buffer_size_, buffer_size_,
                        5000, NULL);

    if (h == INVALID_HANDLE_VALUE) {
        cb(GetLastError());
        return;
    }
    server_.associate(h, h);

    fConnected = ConnectNamedPipe(h, &Overlapped->m_Overlapped);
    if (!fConnected && GetLastError() != ERROR_IO_PENDING) {
        cb(GetLastError());
    }
}

reactor::reactor()
{
    completion_port_ =
        CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);
}

reactor::~reactor()
{
}

void reactor::run()
{
    DWORD dwTransferByte = 0;
    inl::OVERLAPPEDPLUS* pOverlapPlus = NULL;
    LPOVERLAPPED lpOverlapped = NULL;
    struct ClientContext* lpClientContext = NULL;

    while (TRUE) {
        BOOL bOK = ::GetQueuedCompletionStatus(
            completion_port_, &dwTransferByte, (LPDWORD) & lpClientContext,
            &lpOverlapped, 1000);

        int nErr = GetLastError();

        if (!bOK && WAIT_TIMEOUT == nErr) {
            continue;
        }

        pOverlapPlus =
            CONTAINING_RECORD(lpOverlapped, inl::OVERLAPPEDPLUS, m_Overlapped);

        if (pOverlapPlus->callback) pOverlapPlus->callback(bOK ? 0 : nErr);

        if (pOverlapPlus->io_callback)
            pOverlapPlus->io_callback(bOK ? 0 : nErr, dwTransferByte);

        if (pOverlapPlus) {
            free_ovl(pOverlapPlus);
        }
    }
}

void reactor::associate(HANDLE h, void* context)
{
    CreateIoCompletionPort(h, completion_port_, (ULONG_PTR)context, 0);
}

inl::OVERLAPPEDPLUS* reactor::alloc_ovl(inl::IOType type)
{
    return overlapped_pool_.construct(type);
    //return new inl::OVERLAPPEDPLUS(type);
}

void reactor::free_ovl(inl::OVERLAPPEDPLUS* p)
{
    overlapped_pool_.destroy(p);
    //delete p;
}
}
}
