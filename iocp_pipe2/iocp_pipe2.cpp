// iocp_pipe2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread.hpp>
#include <boost/enable_shared_from_this.hpp>
#include "iocp_pipe_server.h"

class session : public boost::enable_shared_from_this<session>,
                public boost::noncopyable
{
  public:
    session(iocp::pipe::reactor& r) : reactor_(r), pipe_(r)
    {
        bytes_ = 0;
        time_ = 0;
        tick_ = GetTickCount();
    }

    virtual ~session()
    {
        wprintf(L"session destroy\n");
    }

    HANDLE& handle()
    {
        return pipe_.handle();
    }

    void post_recv()
    {
        pipe_.async_read(
            buffer_, 8192,
            boost::bind(&session::handler_read, shared_from_this(), _1, _2));
    }

    void start()
    {
        post_recv();
    }

    void stop()
    {
        boost::recursive_mutex::scoped_lock lock(mutex_);
        pipe_.stop();
    }

  protected:
    void handler_written(int error, size_t byteWritten)
    {
        if (!error) {
            post_recv();
        } else {
            stop();
        }
    }
    void handler_read(int error, size_t transfer_bytes)
    {
        if (!error) {
            boost::recursive_mutex::scoped_lock lock(mutex_);
            bytes_ += transfer_bytes;
            if (GetTickCount() - tick_ > 1000) {
                wprintf(L"TID: %d, PID: %d, %0.2f MB\n", GetCurrentThreadId(),
                        pipe_.remote_process_id(),
                        (double)bytes_ / 1024.0 / 1024.0);
                tick_ = GetTickCount();
                bytes_ = 0;
                time_++;
            }

            if (time_ > 30) {
                wprintf(L"TID: %d, PID: %d disconnect\n", GetCurrentThreadId(),
                        pipe_.remote_process_id());
                stop();
            } else {
                // post_recv();
                pipe_.async_write(buffer_, transfer_bytes,
                                  boost::bind(&session::handler_written,
                                              shared_from_this(), _1, _2));
            }

        } else {
            stop();
        }
    }

  private:
    iocp::pipe::reactor& reactor_;
    iocp::pipe::pipe pipe_;
    boost::recursive_mutex mutex_;
    char buffer_[8192];

    LONG bytes_;
    LONG tick_;
    LONG time_;

    iocp::pipe::buffer recv_buffer_;
};
typedef boost::shared_ptr<session> session_ptr;

class server
{
  public:
    server(iocp::pipe::reactor& r)
        : reactor_(r), acceptor_(r, L"\\\\.\\pipe\\TestIOCPPipe", 8192)
    {
        start_acceptor();
    }

    void start_acceptor()
    {
        session_ptr ss = boost::make_shared<session>(reactor_);
        acceptor_.async_accept(
            ss->handle(), boost::bind(&server::handler_accept, this, ss, _1));
    }

    void handler_accept(session_ptr ss, int error)
    {
        if (!error) {
            wprintf(L"client connect\n");
            ss->start();
            start_acceptor();
        }
    }

  private:
    iocp::pipe::acceptor acceptor_;
    iocp::pipe::reactor& reactor_;
};
int _tmain(int argc, _TCHAR* argv[])
{
    iocp::pipe::reactor reactor;
    server srv(reactor);
    boost::thread_group group;

    // for (int i = 0; i < 100; i++) {
    //     group.create_thread(boost::bind(&iocp::pipe::reactor::run,
    // &reactor));
    //}
    // group.join_all();

    reactor.run();
    return 0;
}
